/*
 * 2021-06-07
 */
package academy.learnprogramming.bbbms.service;


/**
 * @author Daniel Kruse
 *
 */
public interface DemoService {
    String getMessage();
}
