/*
 * 2021-06-07
 */
package academy.learnprogramming.bbbms.service;

import org.springframework.stereotype.Service;

/**
 * @author Daniel Kruse
 *
 */
@Service
public class DemoServiceImpl implements DemoService {
    
    @Override
    public String getMessage() {
        return "Hello from the core module";
    }
    
}
