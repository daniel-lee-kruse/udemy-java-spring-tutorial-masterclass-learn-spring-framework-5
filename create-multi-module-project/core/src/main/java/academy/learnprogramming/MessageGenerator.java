/**
 * 
 */
package academy.learnprogramming;

/**
 * @author dlk01
 *
 */
public interface MessageGenerator {
    
    String getMainMessage();
    
    String getResultMessage();
}
