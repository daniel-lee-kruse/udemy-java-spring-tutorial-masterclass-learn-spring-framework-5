/*
 * 
 */
package academy.learnprogramming;

/**
 * @author dlk01
 *
 */
public interface NumberGenerator {
    
    int next();
    
    int getMaxNumber();
    
    int getMinNumber();
}
