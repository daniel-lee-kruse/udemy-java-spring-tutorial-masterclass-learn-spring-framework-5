/**
 * 
 */
package academy.learnprogramming;

import javax.annotation.PostConstruct;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author dlk01
 *
 */
@Slf4j
@Component
public class MessageGeneratorImpl implements MessageGenerator {

	// == constants ==
	private static final String MAIN_MESSAGE = "game.main.message";
	private static final String RESULT_MESSAGE_WON = "game.result.message.won";
	private static final String RESULT_MESSAGE_LOST = "game.result.message.lost";
	private static final String RESULT_MESSAGE_INVALID_RANGE = "game.result.message.invalid.range";
	private static final String RESULT_MESSAGE_GUESS_FIRST = "game.result.message.guess.first";
	private static final String RESULT_MESSAGE_GUESS_LEFT = "game.result.message.guess.left";
	private static final String GAME_HIGHER = "game.higher";
	private static final String GAME_LOWER = "game.lower";

	// == fields ==
	private final Game game;
	private final MessageSource messageSource;

	// == constructors ==
	public MessageGeneratorImpl(Game game, MessageSource messageSource) {
		this.game = game;
		this.messageSource = messageSource;
	}

	// == init ==
	@PostConstruct
	public void init() {
		log.info("game = {}", game);
	}

	// == public methods ==
	@Override
	public String getMainMessage() {
		return getMessage(MAIN_MESSAGE, game.getSmallest(), game.getBiggest());
	}

	@Override
	public String getResultMessage() {
		
		if (game.isGameWon()) {
			return getMessage(RESULT_MESSAGE_WON, game.getNumber());
		} else if (game.isGameLost()) {
			return getMessage(RESULT_MESSAGE_LOST, game.getNumber());
		} else if (!game.isValidNumberRange()) {
			return getMessage(RESULT_MESSAGE_INVALID_RANGE);
		} else if (game.getRemainingGuesses() == game.getGuessCount()) {
			return getMessage(RESULT_MESSAGE_GUESS_FIRST);
		} else {
			String direction = getMessage(GAME_LOWER);
			
			if (game.getGuess() < game.getNumber()) {
				direction = getMessage(GAME_HIGHER);
			}

			return getMessage(RESULT_MESSAGE_GUESS_LEFT, direction, game.getRemainingGuesses());
		}
	}

	// == private methods ==
	private String getMessage(String code, Object... args) {
		return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
	}

}
