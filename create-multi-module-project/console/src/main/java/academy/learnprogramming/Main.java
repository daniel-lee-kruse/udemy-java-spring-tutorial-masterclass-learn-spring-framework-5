/**
 * 
 */
package academy.learnprogramming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

/**
 * @author dlk01
 *
 */
@Slf4j
@SpringBootApplication
public class Main {
	
	public static void main(final String[] args) {
		log.info("Guess the Number Game");
		SpringApplication.run(Main.class, args);
	}
}
