/**
 * 
 */
package academy.learnprogramming.util.thymeleaf;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;

import lombok.extern.slf4j.Slf4j;

/**
 * The course is using 2.0.3 of spring-boot-starter-parent. I'm using 2.4.1.
 * 
 * @author Daniel Kruse
 * 
 */
@Slf4j
@Component
public class DecoupledLogicSetup {
    
    // == fields ==
    private final SpringResourceTemplateResolver templateResolver;
    
    // == constructors ==
    public DecoupledLogicSetup(SpringResourceTemplateResolver templateResolver) {
        this.templateResolver = templateResolver;
    }
    
    // == init ==
    @PostConstruct
    public void init() {
        log.info("** Decoupled template logic is enabled: {} **", templateResolver.getUseDecoupledLogic());
        templateResolver.setUseDecoupledLogic(true);
        log.info("Decoupled template logic enabled");
    }
}
