/**
 * 
 */
package academy.learnprogramming.util;

/**
 * @author Daniel Kruse
 *
 */
public final class ViewNames {
    
    // == constants ==
    public static final String HOME = "home";
    public static final String PLAY = "play";
    public static final String GAME_OVER = "game-over";
    
    // == constructors ==
    private ViewNames() {
    }
}
