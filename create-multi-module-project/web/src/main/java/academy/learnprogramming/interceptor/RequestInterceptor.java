/**
 * 
 */
package academy.learnprogramming.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Daniel Kruse
 *
 */
@Slf4j
public class RequestInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
		log.debug("preHandle method called. handler is {}.", handler);
		log.debug("URL is {}.", request.getRequestURL());
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {
		log.debug("postHandle method called. handler is {}.", handler);
		log.debug("URL is {}.", request.getRequestURL());
		log.debug("model is {}.", modelAndView != null ? modelAndView.getModel() : "modelAndView is null!");
		log.debug("view is {}.", modelAndView != null ? modelAndView.getViewName() : "modelAndView is null!");
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {
		log.debug("afterCompletion method called. handler is {}.", handler);
		log.debug("URL is {}.", request.getRequestURL());
	}

}
