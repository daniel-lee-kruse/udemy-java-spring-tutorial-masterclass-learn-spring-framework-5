/**
 * 
 */
package academy.learnprogramming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Daniel Kruse
 *
 */
@SpringBootApplication
public class WebMain {
    
    public static void main(String[] args) {
        SpringApplication.run(WebMain.class, args);
    }
}
