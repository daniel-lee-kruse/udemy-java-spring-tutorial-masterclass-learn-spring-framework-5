/**
 * 
 */
package academy.learnprogramming.util;

/**
 * @author Daniel Kruse
 *
 */
public final class AttributeNames {
	// == constants ==
	public static final String TODO_ITEM = "todoItem";
	
    // == constructors ==
	private AttributeNames() {}
}
