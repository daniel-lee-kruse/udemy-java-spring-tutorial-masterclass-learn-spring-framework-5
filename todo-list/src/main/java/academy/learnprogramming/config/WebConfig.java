/**
 * 
 */
package academy.learnprogramming.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import academy.learnprogramming.util.ViewNames;

/**
 * @author Daniel Kruse
 *
 */
@EnableWebMvc
@Configuration
@ComponentScan("academy.learnprogramming")
public class WebConfig implements WebMvcConfigurer {
    // == constants ==
	public static final String RESOLVER_PREFIX = "/WEB-INF/view/";
	public static final String RESOVER_SUFFIX = ".jsp";
	
	// == bean methods ==
	@Bean
	public ViewResolver viewResolver() {
		UrlBasedViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix(RESOLVER_PREFIX);
		viewResolver.setSuffix(RESOVER_SUFFIX);
		return viewResolver;
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName(ViewNames.HOME);
	}
	
}
