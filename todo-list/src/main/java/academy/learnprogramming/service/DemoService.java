/**
 * 
 */
package academy.learnprogramming.service;

/**
 * @author Daniel Kruse
 *
 */
public interface DemoService {
    String getHelloMessage(String user);
    String getWelcomeMessage();
}
