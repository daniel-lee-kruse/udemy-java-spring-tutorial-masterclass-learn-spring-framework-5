/**
 * 
 */
package academy.learnprogramming.service;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Daniel Kruse
 *
 */
@Slf4j
@Service
public class DemoServiceImpl implements DemoService {

	@Override
	public String getHelloMessage(String user) {
		log.info("getHelloMessaget(): user={}", user);
		return "Hello " + user;
	}

	@Override
	public String getWelcomeMessage() {
		log.info("getWelcomeMessage()");
		return "Welcome to the Demo application.";
	}

}
