# Java Spring Tutorial Masterclass Learn Spring Framework 5
This work is based on Tim Buchalka's course on Udemy.

Projects are:
- Guess the number game (console based)
- Guess the number game (web based)
- Todo list (web based)
