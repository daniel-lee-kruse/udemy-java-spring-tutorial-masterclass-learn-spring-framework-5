/*
 * 2021-06-02
 */
package academy.learnprogramming.gradledemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Daniel Kruse
 *
 */
@Slf4j
@Controller
public class WelcomeController {
    @GetMapping("welcome")
    public String welcome(Model model) {
        log.info("welcome method call");
        model.addAttribute("message", "Hello Gradle");
        return "welcome";
    }
}
