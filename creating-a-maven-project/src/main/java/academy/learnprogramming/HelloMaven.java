/**
 * This is a udemy.com course.
 */
package academy.learnprogramming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dlk01
 *
 */
public class HelloMaven {
	private final static Logger logger = LoggerFactory.getLogger(HelloMaven.class);
	
	public static void main(String[] args) {
//		System.out.println("Hello Maven");
		logger.info("Hello Info");
		logger.debug("Hello Debug");
	}
}
